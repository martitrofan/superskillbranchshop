package com.skillbranch.bestshop.di.components;

import javax.inject.Singleton;

import dagger.Component;
import com.skillbranch.bestshop.data.managers.DataManager;
import com.skillbranch.bestshop.di.modules.LocalModule;
import com.skillbranch.bestshop.di.modules.NetworkModule;

@Component(dependencies = AppComponent.class, modules = {NetworkModule.class, LocalModule.class})
@Singleton
public interface DataManagerComponent {
    void inject(DataManager dataManager);
}
