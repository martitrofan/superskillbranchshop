package com.skillbranch.bestshop.di.modules;

import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import com.skillbranch.bestshop.data.managers.PreferencesManager;

@Module
public class LocalModule extends FlavorLocalModule {
    @Provides
    @Singleton
    PreferencesManager providePreferencesManager(Context context) {
        return new PreferencesManager(context);
    }

//    @Provides
//    @Singleton
//    RealmManager provideRealmManager() {
//        return new RealmManager();
//    }
}
