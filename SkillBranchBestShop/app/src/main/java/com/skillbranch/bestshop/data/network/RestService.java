package com.skillbranch.bestshop.data.network;

import java.util.List;

import com.skillbranch.bestshop.data.network.res.AvatarUrlRes;
import com.skillbranch.bestshop.data.network.res.ProductRes;
import com.skillbranch.bestshop.data.network.res.models.AddCommentRes;
import com.skillbranch.bestshop.data.network.res.models.Comments;
import com.skillbranch.bestshop.utils.ConstantManager;
import okhttp3.MultipartBody;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import rx.Observable;

public interface RestService {
    @GET("products")
    Observable<Response<List<ProductRes>>> getProductResObs (@Header(ConstantManager.IF_MODIFIED_SINCE_HEADER) String lastEntityUpdate);

    @POST("products/{id}/comments")
    Observable<Comments> sendCommentToServer(@Path("id") String id,
                                        @Body AddCommentRes post);

    @Multipart
    @POST("avatar")
    Observable<AvatarUrlRes> uploadUserAvatar(@Part MultipartBody.Part file);
}
